/** \file contacts.c
 *
 *  \brief   This file provides the implementation of the functions
 *           to manage the contact graph.
 *
 *
 ** \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of Unibo-CGR.                                            <br>
 **                                                                               <br>
 **    Unibo-CGR is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    Unibo-CGR is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with Unibo-CGR.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \author Lorenzo Persampieri, lorenzo.persampieri@studio.unibo.it
 *
 *  \par Supervisor
 *       Carlo Caini, carlo.caini@unibo.it
 */

#include "contacts.h"

#include <stdlib.h>

#include "../../library/commonDefines.h"
#include "../../library/list/list.h"
#include "../../library_from_ion/rbt/rbt.h"
#include "../../routes/routes.h"

/**
 * \brief Get the absolute value of "a"
 *
 * \param[in]   a   The real number for which we want to know the absolute value
 *
 * \hideinitializer
 */
#define absolute(a) (((a) < 0) ? (-(a)) : (a))


static void erase_contact(Contact*);

static void erase_contact_note(ContactNote *note);
static ContactNote* create_contact_note();

/**
 * \brief This struct is used to keep in one place all the data used by
 *        the contact graph library.
 */
struct ContactSAP {
	/**
	 * \brief The contact graph.
	 */
	Rbt *contacts;
	/**
	 * \brief The time of the next contact that expires.
	 */
	time_t timeContactToRemove;

};

/******************************************************************************
 *
 * \par Function Name:
 *      ContactSAP_open
 *
 * \brief  Allocate memory for the contacts graph structure (rbt)
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return int
 *
 * \retval   0   Success case: contacts graph created
 * \retval  -2   Error case: contacts graph cannot be created due to MWITHDRAW error
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *  21/10/22 | L. Persampieri  |  Renamed function
 *****************************************************************************/
int ContactSAP_open(UniboCGRSAP* uniboCgrSap)
{
	if (UniboCGRSAP_get_ContactSAP(uniboCgrSap)) return 0;

    ContactSAP* sap = MWITHDRAW(sizeof(ContactSAP));
    if (!sap) { return -2; }
    UniboCGRSAP_set_ContactSAP(uniboCgrSap, sap);
    memset(sap, 0, sizeof(ContactSAP));
    sap->timeContactToRemove = MAX_POSIX_TIME;
    sap->contacts = rbt_create(free_contact, compare_contacts);
    if (!sap->contacts) {
        ContactSAP_close(uniboCgrSap);
        return -2;
    }

    return 0;
}

void ContactSAP_decrease_time(UniboCGRSAP* uniboCgrSap, time_t diff) {
    Contact *current;
    RbtNode *node;
    ContactSAP* contactSap = UniboCGRSAP_get_ContactSAP(uniboCgrSap);
    contactSap->timeContactToRemove = MAX_POSIX_TIME;
    for (current = get_first_contact(uniboCgrSap, &node); current != NULL; current = get_next_contact(&node))
    {
        current->fromTime -= diff;
        current->toTime -= diff;

        if (current->toTime < contactSap->timeContactToRemove) {
            contactSap->timeContactToRemove = current->toTime;
        }
    }
}


/******************************************************************************
 *
 * \par Function Name:
 *      discardAllRoutesFromContactsGraph
 *
 * \brief Delete all the citations for every contact that belongs to the contacts graph
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return void
 *
 * \par Notes:
 *              1. This function doesn't delete any route.
 *              2. If you call this function you must call even the correspective function for the nodes tree.
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
void discardAllRoutesFromContactsGraph(UniboCGRSAP* uniboCgrSap)
{
	Contact *current;
	RbtNode *node;
	delete_function delete_fn;

	for (current = get_first_contact(uniboCgrSap, &node); current != NULL; current = get_next_contact(&node))
	{
		delete_fn = current->citations->delete_data_elt;
		current->citations->delete_data_elt = NULL;
		free_list_elts(current->citations);
		current->citations->delete_data_elt = delete_fn;

	}
}

/******************************************************************************
 *
 * \par Function Name:
 *      removeExpiredContacts
 *
 * \brief  Delete all the contacts that have a toTime field <= than the time passed as argument.
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return void
 *
 * \par Notes:
 *              1. For every expired contact we will call the deleteFn, actually that means:
 *                 all routes where the expired contact appears will be deleted.
 *              2. The timeContactToRemove will be redefined.
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
void removeExpiredContacts(UniboCGRSAP* uniboCgrSap)
{
	time_t min = MAX_POSIX_TIME;
	Contact *contact;
    const time_t time = UniboCGRSAP_get_current_time(uniboCgrSap);
	RbtNode *node, *next;
#if DEBUG_CGR
	unsigned int tot = 0;
#endif
	ContactSAP *sap = UniboCGRSAP_get_ContactSAP(uniboCgrSap);


	if (time >= sap->timeContactToRemove)
	{
		debug_printf("Remove the expired contacts.");
		node = rbt_first(sap->contacts);
		while (node != NULL)
		{
			next = rbt_next(node);
			if (node->data != NULL)
			{
				contact = (Contact*) node->data;

				if (contact->toTime <= time)
				{
					rbt_delete(sap->contacts, contact);
#if DEBUG_CGR
					tot++;
#endif
				}
				else if (contact->toTime < min)
				{
					min = contact->toTime;
				}
			}

			node = next;
		}

		sap->timeContactToRemove = min;

		debug_printf("Removed %u contacts, next remove contacts time: %ld", tot,
				(long int ) sap->timeContactToRemove);
	}
}

/******************************************************************************
 *
 * \par Function Name:
 *      compare_contacts
 *
 * \brief  Compare two contacts
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return int
 *
 * \retval   0  The contacts are equals (even if they has the same pointed address)
 * \retval  -1  The first contact is less than the second contact
 * \retval   1  The first contact is greater than the second contact
 *
 * \param[in]	*first    The first contact
 * \param[in]	*second   The second contact
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
int compare_contacts(void *first, void *second)
{
	Contact *a, *b;
	int result = 0;

	if (first == second) //same address pointed
	{
		result = 0;
	}
	else if (first != NULL && second != NULL)
	{
		a = (Contact*) first;
		b = (Contact*) second;

		if (a->fromNode < b->fromNode)
		{
			result = -1;
		}
		else if (a->fromNode > b->fromNode)
		{
			result = 1;
		}
		else if (a->toNode < b->toNode)
		{
			result = -1;
		}
		else if (a->toNode > b->toNode)
		{
			result = 1;
		}
		else if (a->fromTime < b->fromTime)
		{
			result = -1;
		}
		else if (a->fromTime > b->fromTime)
		{
			result = 1;
		}
		else
		{
			result = 0;
		}
	}

	return result;
}

/**
 * \brief modify contact start time and update MTVs (accordingly to new start time -- keep booking information)
 * \retval 0 success
 * \retval -1 contact not found
 * \retval -2 arguments error
 * \retval -3 cannot insert overlapping contacts
 */
int revise_contact_start_time(UniboCGRSAP* uniboCgrSap, uint64_t fromNode, uint64_t toNode, time_t fromTime, time_t newFromTime) {
    RbtNode* contact_node;
    Contact *contact = get_contact(uniboCgrSap, fromNode, toNode, fromTime, &contact_node);
    if (contact == NULL) {
        return -1; // contact not found
    }
    if (newFromTime > contact->toTime) {
        return -2;
    }

    // note that since we are changing start time we may overlap
    // only with a contact the compares lower than the current contact.
    // so we need only to check if the previous contact has the same sender
    // and receiver and if its end time is greater than the new start time.

    RbtNode* prev_contact_node = rbt_prev(contact_node);
    if (!prev_contact_node || !prev_contact_node->data) {
        // ok no overlapping
    } else {
        Contact* prev_contact = prev_contact_node->data;

        if (prev_contact->fromNode == contact->fromNode
        && prev_contact->toNode == contact->toNode
        && prev_contact->toTime > newFromTime) {
            // will overlap
            return -3;
        }
    }

    const double max_new_mtv = (contact->toTime - newFromTime)*contact->xmitRate;
    for (int i = 0; i < 3; i++) {
        // reset MTVs to max values
        contact->mtv[i] = max_new_mtv;
    }
    contact->fromTime = newFromTime;
    return 0;
}
/**
 * \brief modify contact end time and update MTVs (accordingly to new end time -- keep booking information)
 * \retval 0 success
 * \retval -1 contact not found
 * \retval -2 arguments error
 * \retval -3 cannot insert overlapping contacts
 */
int revise_contact_end_time(UniboCGRSAP* uniboCgrSap, uint64_t fromNode, uint64_t toNode, time_t fromTime, time_t newEndTime) {
    RbtNode* contact_node;
    Contact *contact = get_contact(uniboCgrSap, fromNode, toNode, fromTime, &contact_node);
    if (contact == NULL) {
        return -1; // contact not found
    }
    if (newEndTime < contact->fromTime) {
        return -2;
    }

    // note that since we are changing end time we may overlap
    // only with a contact the compares greater than the current contact.
    // so we need only to check if the next contact has the same sender
    // and receiver and if its start time is lower than the new end time.

    RbtNode* next_contact_node = rbt_next(contact_node);
    if (!next_contact_node || !next_contact_node->data) {
        // ok no overlapping
    } else {
        Contact* next_contact = next_contact_node->data;

        if (next_contact->fromNode == contact->fromNode
            && next_contact->toNode == contact->toNode
            && next_contact->fromTime < newEndTime) {
            // will overlap
            return -3;
        }
    }

    const double max_new_mtv = (newEndTime - contact->fromTime)*contact->xmitRate;
    for (int i = 0; i < 3; i++) {
        // reset MTVs to max values
        contact->mtv[i] = max_new_mtv;
    }
    contact->toTime = newEndTime;

    struct ContactSAP* contactSap = UniboCGRSAP_get_ContactSAP(uniboCgrSap);
    if (contact->toTime < contactSap->timeContactToRemove) {
        contactSap->timeContactToRemove = contact->toTime;
    }
    return 0;
}


/******************************************************************************
 *
 * \par Function Name:
 *      revise_confidence
 *
 * \brief  Revise the contact's confidence
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return int
 *
 * \retval   0  Confidence revised
 * \retval  -1  Contact not found
 * \retval  -2  Arguments error
 *
 * \param[in]	   fromNode     The contact's sender node
 * \param[in]        toNode     The contact's receiver node
 * \param[in]      fromTime     The contact's start time
 * \param[in] newConfidence     The revised confidence
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
int revise_confidence(UniboCGRSAP* uniboCgrSap, uint64_t fromNode, uint64_t toNode, time_t fromTime, float newConfidence)
{
	int result = -2;
	Contact *contact = NULL;

	if (fromNode != 0 && toNode != 0 && fromTime >= 0 && newConfidence >= 0.0
			&& newConfidence <= 1.0)
	{
		contact = get_contact(uniboCgrSap, fromNode, toNode, fromTime, NULL);
		result = -1;
		if(contact != NULL)
		{
			contact->confidence = newConfidence;
			result = 0;
		}
	}

	return result;
}

/******************************************************************************
 *
 * \par Function Name:
 *      revise_xmit_rate
 *
 * \brief  Revise the contact's transmit rate (and MTVs)
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return int
 *
 * \retval   0  xmitRate revised
 * \retval  -1  Contact not found
 * \retval  -2  Arguments error
 *
 * \param[in]	   fromNode     The contact's sender node
 * \param[in]        toNode     The contact's receiver node
 * \param[in]      fromTime     The contact's start time
 * \param[in]      xmitRate     The revised transmit rate
 * \param[in]       copyMTV     Set to 1 if you want to copy MTVs.
 *                              Set to 0 otherwise
 * \param[in]           mtv     An array of 3 elements. The MTVs copied if copyMTV is setted to 1.
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
int revise_xmit_rate(UniboCGRSAP* uniboCgrSap, uint64_t fromNode, uint64_t toNode, time_t fromTime, uint64_t xmitRate)
{
	int result = -2;
	Contact *contact = NULL;

	if (fromNode != 0 && toNode != 0 && fromTime >= 0)
	{
		contact = get_contact(uniboCgrSap, fromNode, toNode, fromTime, NULL);
		result = -1;
		if(contact != NULL)
		{
			contact->xmitRate = xmitRate;
            const double max_new_mtv = (contact->toTime - contact->fromTime)*xmitRate;
            for (int i = 0; i < 3; i++) {
                // reset MTVs to max values
                contact->mtv[i] = max_new_mtv;
            }
			result = 0;
		}
	}

	return result;
}

/******************************************************************************
 *
 * \par Function Name:
 * 		get_contact_with_time_tolerance
 *
 * \brief Search a contact into contact graph. The contact has to match with
 *        the id {fromNode, toNode, fromTime}.
 *
 * \details contact->fromTime can be included in
 *          fromTime - tolerance <= contact->fromTime <= fromTime + tolerance
 *
 * \par Date Written:
 * 		23/04/20
 *
 * \return Contact*
 *
 * \retval  Contact*   The contact found
 * \retval  NULL       Contact not found
 *
 * \param[in]      fromNode     The sender node of the contact
 * \param[in]      toNode       The receiver node of the contact
 * \param[in]      fromTime     The start time of the contact
 * \param[in]      tolerance    Time tolerance
 *
 * \par Revision History:
 *
 *  DD/MM/YY | AUTHOR          |   DESCRIPTION
 *  -------- | --------------- |  -----------------------------------------------
 *  23/04/20 | L. Persampieri  |   Initial Implementation and documentation.
 *****************************************************************************/
Contact * get_contact_with_time_tolerance(UniboCGRSAP* uniboCgrSap, uint64_t fromNode, uint64_t toNode, time_t fromTime, unsigned int tolerance)
{
	Contact *result = NULL;
	Contact *current;
	RbtNode *node;
	int stop = 0;
	time_t difference;

	for(current = get_first_contact_from_node_to_node(uniboCgrSap, fromNode, toNode, &node);
			current != NULL && !stop; current = get_next_contact(&node))
	{
		if(current->fromNode == fromNode && current->toNode == toNode)
		{
			// difference in absolute value
			difference = absolute(current->fromTime - fromTime);

			if(difference <= tolerance)
			{
				result = current;
				stop = 1;
			}
			else if(current->fromTime > fromTime + tolerance)
			{
				// not found
				stop = 1;
			}
		}
		else
		{
			// not found
			stop = 1;
		}
	}

	return result;
}

/******************************************************************************
 *
 * \par Function Name:
 *      erase_contact
 *
 * \brief  Reset all the contact's fields
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return void
 *
 * \param[in]	*contact   The contact for which we want to erase all fields
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
static void erase_contact(Contact *contact)
{
    memset(contact, 0, sizeof(Contact));
    contact->type = TypeScheduled;
}

/******************************************************************************
 *
 * \par Function Name:
 *      reset_ContactsGraph
 *
 * \brief Delete any contact that belongs to the graph, but not the contacts graph structure
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return void
 *
 * \par Notes:
 *             1. The deleteFn will be called, actually that means:
 *                all routes where a contact appears will be deleted.
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
void reset_ContactsGraph(UniboCGRSAP* uniboCgrSap)
{
	ContactSAP *sap = UniboCGRSAP_get_ContactSAP(uniboCgrSap);

	rbt_clear(sap->contacts);
	sap->timeContactToRemove = MAX_POSIX_TIME;
}

/******************************************************************************
 *
 * \par Function Name:
 *      ContactSAP_close
 *
 * \brief  Delete any contact that belongs to the graph, and the contacts graph itself.
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return  void
 *
 * \par Notes:
 *             1. The deleteFn will be called, actually that means:
 *                all routes where a contact appears will be deleted.
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *  21/10/22 | L. Persampieri  |  Renamed function
 *****************************************************************************/
void ContactSAP_close(UniboCGRSAP* uniboCgrSap)
{
    if (!UniboCGRSAP_get_ContactSAP(uniboCgrSap)) return;

	ContactSAP *sap = UniboCGRSAP_get_ContactSAP(uniboCgrSap);

	rbt_destroy(sap->contacts);

    memset(sap, 0, sizeof(ContactSAP));
    MDEPOSIT(sap);
    UniboCGRSAP_set_ContactSAP(uniboCgrSap, NULL);
}

/******************************************************************************
 *
 * \par Function Name:
 *      erase_contact_note
 *
 * \brief  Erase all the fields of the ContactNote passed as argument
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return void
 *
 * \param[in]	*note  The ContactNote for which we want to erase all fields.
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
static void erase_contact_note(ContactNote *note)
{

	if (note != NULL)
	{
        memset(note, 0, sizeof(ContactNote));
        note->arrivalTime = -1;
	}
}

/******************************************************************************
 *
 * \par Function Name:
 *      create_contact_note
 *
 * \brief  Allocate (with MWITHDRAW) a memory area for a ContactNote
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return ContactNote*
 *
 * \retval ContactNote*  The new allocated ContactNote
 * \retval NULL          MWITHDRAW error)
 *
 * \par Notes:
 *              1. The new allocated ContactNote will have all fields erased.
 *              2. You must check that the return value of this function is not NULL.
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
static ContactNote* create_contact_note()
{
	ContactNote *note = (ContactNote*) MWITHDRAW(sizeof(ContactNote));

	if (note != NULL)
	{
		erase_contact_note(note);
	}

	return note;
}

/******************************************************************************
 *
 * \par Function Name:
 *      free_contact
 *
 * \brief  Delete the contact pointed by data
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return  void
 *
 * \param[in]	*data   The contact that we want to delete
 *
 * \par Notes:
 *              1. This function delete every route where the contact (data) appears.
 *              2. This function will delete the contact in its totality, included the
 *                 ContactNote and citations's list.
 *              3. The delete function used to deallocate memory is: MDEPOSIT.
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
void free_contact(void *data)
{
	Contact *contact;
	ListElt *current, *temp;
	ListElt *hop;
	Route *route;
	int deleted;

	if (data != NULL)
	{
		contact = (Contact*) data;
		if (contact->routingObject != NULL)
		{
			MDEPOSIT(contact->routingObject);
		}

		if (contact->citations != NULL)
		{
			current = contact->citations->first;
			while (current != NULL)
			{
				deleted = 0;
				temp = current->next;

				if (current->data != NULL)
				{
					hop = (ListElt*) current->data;
					if (hop->list != NULL)
					{
						if (hop->list->userData != NULL)
						{
							deleted = 1;
							route = (Route*) hop->list->userData;
							delete_cgr_route(route); //this function remove the citation
						}
					}
				}
				if (deleted == 0)
				{
					flush_verbose_debug_printf("Error!!!");
					list_remove_elt(current);
				}

				current = temp;
			}

			MDEPOSIT(contact->citations);

		}
		erase_contact(contact);
		MDEPOSIT(contact);
		data = NULL;
	}
}

/******************************************************************************
 *
 * \par Function Name:
 *      create_contact
 *
 * \brief  Allocate memory for a new contact
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return Contact*
 *
 * \retval Contact*  The pointer to the allocated contact 
 * \retval NULL      MWITHDRAW error
 *
 * \param[in]	fromNode      The contact's sender node
 * \param[in]	toNode        The contact's receiver node
 * \param[in]	fromTime      The contact's start time
 * \param[in]	toTime        The contact's end time
 * \param[in]	xmitRate      In bytes per second
 * \param[in]	confidence    The confidence that the contact will effectively materialize
 * \param[in]	type          The type of the contact (Registration or Scheduled)
 *
 * \par Notes:
 *             1. You must check that the return value of this function is not NULL.
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
Contact* create_contact(uint64_t fromNode, uint64_t toNode, time_t fromTime,
		time_t toTime, uint64_t xmitRate, float confidence, CtType type)
{
	Contact *contact = NULL;
	double volume;
	contact = (Contact*) MWITHDRAW(sizeof(Contact));

	if (contact != NULL)
	{
		contact->fromNode = fromNode;
		contact->toNode = toNode;
		contact->fromTime = fromTime;
		contact->toTime = toTime;
		contact->xmitRate = xmitRate;
		contact->confidence = confidence;
		contact->type = type;
		/* NOTE: We assume that toTime - fromTime is greater or equal to 0 */
		volume = (double) (xmitRate * ((uint64_t) (toTime - fromTime)));
		contact->mtv[0] = volume;
		contact->mtv[1] = volume;
		contact->mtv[2] = volume;

		contact->citations = list_create(contact, NULL, NULL, NULL);
		if (contact->citations == NULL)
		{
			MDEPOSIT(contact);
			contact = NULL;
		}
		else
		{
			contact->routingObject = create_contact_note();
			if (contact->routingObject == NULL)
			{
				MDEPOSIT(contact->citations);
				MDEPOSIT(contact);
				contact = NULL;
			}
		}
	}

	return contact;
}

/******************************************************************************
 *
 * \par Function Name:
 *      add_contact_to_graph
 *
 * \brief  Allocate memory for a contact, set the fields of the contacts to the
 *         passed arguments and add the contact to the contacts graph
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return int
 *
 * \retval   1   Success case: contact added to the contacts graph
 * \retval   0   Arguments error case
 * \retval  -1   Overlapped contact (don't change it)
 * \retval  -2   MWITHDRAW error
 *
 * \param[in]	fromNode           The contact's sender node
 * \param[in]	toNode             The contact's receiver node
 * \param[in]	fromTime           The contact's start time
 * \param[in]	toTime             The contact's end time
 * \param[in]	xmitRate           In bytes per second
 * \param[in]	confidence         The confidence that the contact will effectively materialize
 * \param[in]   copyMTV            Set to 1 if you want to copy the MTV from mtv input parameter.
 *                                 Set to 0 otherwise
 * \param[in]   mtv                The contact's MTV: this must be an array of 3 elements.
 *
 * \par Notes:
 *             1. This function will change timeContactToRemove if the contact->toTime of the
 *                new contact is less than the current timeContactToRemove, in that case
 *                timeContactToRemove will be equals to contact->toTime
 *             2. Set copyMTV to 1 if you want to be compatible with the data gets
 *                for example from an interface.
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
int add_contact_to_graph(UniboCGRSAP* uniboCgrSap, uint64_t fromNode, uint64_t toNode, time_t fromTime,
                         time_t toTime, uint64_t xmitRate, float confidence, int copyMTV, const double mtv[])
{
	int result;
	int overlapped;
	Contact *contact, *temp;
	RbtNode *elt = NULL;
	CtType contactType;
	ContactSAP *sap;

	if (fromNode == 0 || toNode == 0 || toTime < 0 || (fromTime > toTime) || confidence < 0.0
			|| confidence > 1.0)
	{
		result = 0;
	}
	else
	{
		sap = UniboCGRSAP_get_ContactSAP(uniboCgrSap);

		result = -1;
			if (fromTime >= 0) /*Type: Scheduled	*/
			{
				temp = get_first_contact_from_node_to_node(uniboCgrSap, fromNode, toNode, &elt);
				overlapped = 0;
				while (temp != NULL)
				{
					if (temp->fromNode == fromNode && temp->toNode == toNode)
					{
						if(fromTime == temp->fromTime && toTime == temp->toTime)
						{
							// contact exists in contacts graph
							overlapped = 1;
							temp = NULL;
						}
						else if (fromTime >= temp->fromTime && fromTime < temp->toTime)
						{
							overlapped = 1;
							temp = NULL; //I leave the loop
						}
						else if (toTime > temp->fromTime && toTime <= temp->toTime)
						{
							overlapped = 1;
							temp = NULL; //I leave the loop
						}
						else if (toTime <= temp->fromTime)
						{
							//contacts ordered by the fromTime
							temp = NULL; //I leave the loop
						}
						else
						{
							temp = get_next_contact(&elt);
						}
					}
					else
					{
						temp = NULL; //I leave the loop
					}
				}

				if (overlapped == 0)
				{
					contactType = TypeScheduled;
					contact = create_contact(fromNode, toNode, fromTime, toTime, xmitRate, confidence,
							contactType);
                    if (!contact) {
                        return -2;
                    }

					if(copyMTV != 0)
					{
						contact->mtv[0] = mtv[0];
						contact->mtv[1] = mtv[1];
						contact->mtv[2] = mtv[2];
					}
					elt = rbt_insert(sap->contacts, contact);

					result = ((elt != NULL) ? 1 : -2);

					if (result == -2)
					{
						free_contact(contact);
					}
					else if (sap->timeContactToRemove > toTime)
					{
						sap->timeContactToRemove = toTime;
					}
				}
			}
	}

	return result;
}

/******************************************************************************
 *
 * \par Function Name:
 *      remove_contact_elt_from_graph
 *
 * \brief  Remove the contact that is equal to elt, compared with the compare_contacts function
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return  void
 *
 * \param[in]   *elt  The elt's fields must be the {fromNode, toNode, fromTime} of the contact
 *                    that we want to remove from the graph
 *
 * \par Notes:
 *              1. The free_contact will be called for the contact removed.
 *                 Note that if this contact is elt itself, it will be removed and you have
 *                 a dummy reference to it.
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
void remove_contact_elt_from_graph(UniboCGRSAP* uniboCgrSap, Contact *elt)
{
	ContactSAP *sap = UniboCGRSAP_get_ContactSAP(uniboCgrSap);

	if (elt != NULL)
	{
		rbt_delete(sap->contacts, elt);
	}
}

/******************************************************************************
 *
 * \par Function Name:
 *      remove_contact_from_graph
 *
 * \brief  Remove the contact that has the same {fromNode, toNode, fromTime} fields
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return  void
 *
 * \param[in]   fromTime   Contact's start time
 * \param[in]	fromNode   The sender node of the contact to remove
 * \param[in]   toNode     The receiver node of the contact to remove
 *
 * \par Notes:
 *             1. The free_contact will be called for the contact(s) removed.
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
void remove_contact_from_graph(UniboCGRSAP* uniboCgrSap, time_t fromTime, uint64_t fromNode, uint64_t toNode)
{
	Contact arg;
	ContactSAP *sap = UniboCGRSAP_get_ContactSAP(uniboCgrSap);

    erase_contact(&arg);
    arg.fromNode = fromNode;
    arg.toNode = toNode;
    arg.fromTime = fromTime;
    rbt_delete(sap->contacts, &arg);
}

/* ----------------------------------------------------
 * Functions to search contacts in the graph
 * ----------------------------------------------------
 */

/******************************************************************************
 *
 * \par Function Name:
 *      get_contact
 *
 * \brief  Get the contact that matches with the {fromNode, toNode, fromTime}
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return Contact*
 *
 * \retval Contact*  The contact found
 * \retval NULL      There isn't a contact with this characteristics
 *
 * \param[in]   fromNode    The contact's sender node
 * \param[in]   toNode      The contact's receiver node
 * \param[in]   fromTime    The contact's start time
 * \param[out]  **node      If this argument isn't NULL, at the end it will
 *                          contains the RbtNode that points to the contact returned by the function
 *
 * \par Notes:
 *             1. You must check that the return value of this function is not NULL.
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
Contact* get_contact(UniboCGRSAP* uniboCgrSap, uint64_t fromNode, uint64_t toNode, time_t fromTime,
                     RbtNode **node)
{
	Contact arg, *result;
	RbtNode *elt;
	ContactSAP *sap = UniboCGRSAP_get_ContactSAP(uniboCgrSap);

	result = NULL;
	if (fromNode != 0 && toNode != 0 && fromTime >= 0)
	{
		erase_contact(&arg);
		arg.fromNode = fromNode;
		arg.toNode = toNode;
		arg.fromTime = fromTime;

		elt = rbt_search(sap->contacts, &arg, NULL);
		if (elt != NULL)
		{
			if (elt->data != NULL)
			{
				result = (Contact*) elt->data;
				if (node != NULL)
				{
					*node = elt;
				}
			}
		}
	}

	return result;
}

/******************************************************************************
 *
 * \par Function Name:
 *      get_first_contact
 *
 * \brief  Get the first contact of the contacts graph
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return Contact*
 *
 * \retval Contact*  The first contact
 * \retval NULL      The contacts graph is empty
 *
 * \param[out]  **node   If this argument isn't NULL, at the end it will
 *                       contains the RbtNode that points to the contact returned by the function
 *
 * \par Notes:
 *             1. You must check that the return value of this function is not NULL.
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
Contact* get_first_contact(UniboCGRSAP* uniboCgrSap, RbtNode **node)
{
	Contact *result = NULL;
	RbtNode *currentContact = NULL;
	ContactSAP *sap = UniboCGRSAP_get_ContactSAP(uniboCgrSap);

	currentContact = rbt_first(sap->contacts);
	if (currentContact != NULL)
	{
		result = (Contact*) currentContact->data;
		if (node != NULL)
		{
			*node = currentContact;
		}
	}

	return result;
}

/******************************************************************************
 *
 * \par Function Name:
 *      get_first_contact_from_node
 *
 * \brief  Get the first contact of the graph that has contact->fromNode == fromNode
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return Contact*
 *
 * \retval Contact*  The contact found
 * \retval NULL      There isn't a contact with this fromNode field
 *
 * \param[in]   fromNode   The contact's sender node
 * \param[out]  **node     If this argument isn't NULL, at the end it will
 *                         contains the RbtNode that points to the contact returned by the function
 *
 * \par Notes:
 *             1. You must check that the return value of this function is not NULL.
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
Contact* get_first_contact_from_node(UniboCGRSAP* uniboCgrSap, uint64_t fromNode, RbtNode **node)
{
	Contact arg;
	Contact *result = NULL;
	RbtNode *currentContact = NULL;
	ContactSAP *sap = UniboCGRSAP_get_ContactSAP(uniboCgrSap);

	erase_contact(&arg);
	arg.fromNode = fromNode;
	arg.fromTime = -1;
	rbt_search(sap->contacts, &arg, &currentContact);

	if (currentContact != NULL)
	{
		result = (Contact*) currentContact->data;

		if (result->fromNode != fromNode)
		{
			result = NULL;
		}
		else if (node != NULL)
		{
			*node = currentContact;
		}
	}

	return result;
}

/******************************************************************************
 *
 * \par Function Name:
 *      get_first_contact_from_node_to_node
 *
 * \brief Get the first contact of the graph that has
 *        (contact->fromNode == fromNode) && (contact->toNode == toNode)
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return Contact*
 *
 * \retval Contact*  The contact found 
 * \retval NULL      There isn't a contact with this sender node and receiver node
 *
 *
 * \param[in]   fromNode    The contact's sender node
 * \param[in]   toNode      The contact's receiver node
 * \param[out]  **node   If this argument isn't NULL, at the end it will
 *                          contains the RbtNode that points to the contact returned by the function
 *
 * \par Notes:
 *              1. You must check that the return value of this function is not NULL.
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
Contact* get_first_contact_from_node_to_node(UniboCGRSAP* uniboCgrSap, uint64_t fromNode, uint64_t toNode,
                                             RbtNode **node)
{
	Contact arg;
	Contact *result = NULL;
	RbtNode *currentContact = NULL;
	ContactSAP *sap = UniboCGRSAP_get_ContactSAP(uniboCgrSap);

	erase_contact(&arg);
	arg.fromNode = fromNode;
	arg.toNode = toNode;
	arg.fromTime = -1;
	rbt_search(sap->contacts, &arg, &currentContact);

	if (currentContact != NULL)
	{
		result = (Contact*) currentContact->data;

		if (result->fromNode != fromNode || result->toNode != toNode)
		{
			result = NULL;
		}
		else if (node != NULL)
		{
			*node = currentContact;
		}
	}

	return result;
}

/******************************************************************************
 *
 * \par Function Name:
 *      get_next_contact
 *
 * \brief  Get the next contact referring to the current contact pointed by the argument "node"
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return Contact*
 *
 * \retval Contact*  The contact found
 * \retval NULL      There isn't the next contact
 *
 * \param[in,out]  **node  If this arguments isn't NULL, at the end it will
 *                            contains the RbtNode that points to the contact returned by the function
 *
 * \par Notes:
 *              1. You must check that the return value of this function is not NULL.
 *              2. If we find the next contact this function update the RbtNode pointed by node.
 *              3. You can use this function in a delete loop (referring to the current
 *                 implementation of "rbt_delete").
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
Contact* get_next_contact(RbtNode **node)
{
	Contact *result = NULL;
	RbtNode *temp = NULL;

	if (node != NULL)
	{
		temp = rbt_next(*node);
		if (temp != NULL)
		{
			result = (Contact*) temp->data;
		}

		*node = temp;
	}

	return result;
}

/******************************************************************************
 *
 * \par Function Name:
 *      get_prev_contact
 *
 * \brief  Get the previous contact referring to the current contact pointed by the argument "node"
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \retval Contact* The contact found
 * \retval NULL     There isn't the previous contact
 *
 * \param[in,out]  **node  If this arguments isn't NULL, at the end it will
 *                         contains the RbtNode that points to the contact returned by the function
 *
 * \par Notes:
 *              1. You must check that the return value of this function is not NULL.
 *              2. If we find the prev contact this function update the RbtNode pointed by node.
 *              3. Never use this function in a delete loop (referring to the current
 *                 implementation of "rbt_delete").
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
Contact* get_prev_contact(RbtNode **node)
{
	Contact *result = NULL;
	RbtNode *temp = NULL;

	/*
	 * Non usare questa funzione in un ciclo di "delete", per maggiori dettagli guarda la
	 * struttura di "rbt_delete"
	 */

	if (node != NULL)
	{
		temp = rbt_prev(*node);
		if (temp != NULL)
		{
			result = (Contact*) temp->data;
			*node = temp;
		}
		else
		{
			*node = NULL; //temp == NULL
		}
	}

	return result;
}

/******************************************************************************
 *
 * \par Function Name:
 *      printContact
 *
 * \brief  Print the contact pointed by data to a buffer and at the end to_add points to that buffer
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return int
 *
 * \retval      0   Success case
 * \retval     -1   Some errors occurred
 *
 * \param[out]	*file     The file where we want to print the contact's fields
 * \param[in]	*data     The pointer to the contact
 *
 * \par Notes:
 *             1. This function assumes that "to_add" is not NULL.
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
static int printContact(FILE *file, void *data)
{
	Contact *contact;
	int result = -1;
	if (data != NULL && file != NULL)
	{
		result = 0;
		contact = (Contact*) data;
		fprintf(file, "%-15" PRIu64 " %-15" PRIu64 " %-15ld %-15ld %-15" PRIu64 " %-15.2f ", contact->fromNode,
				contact->toNode, (long int) contact->fromTime, (long int) contact->toTime,
				contact->xmitRate, contact->confidence);
		if (contact->citations != NULL)
		{
			fprintf(file, "%lu\n", contact->citations->length);
		}
		else
		{
			fprintf(file, "NULL\n");
		}
	}
	else
	{
		fprintf(file, "\nCONTACT: NULL\n");
	}

	return result;
}

/******************************************************************************
 *
 * \par Function Name:
 *      printContactsGraph
 *
 * \brief  Print the contacts graph
 *
 *
 * \par Date Written:
 *      13/01/20
 *
 * \return int
 *
 * \retval   1   Success case, contacts graph printed
 * \retval   0   The file is NULL
 *
 * \param[in]  file          The file where we want to print the contacts graph
 * \param[in]  currentTime   The time to print together at the contacts graph to keep trace of the
 *                           modification's history
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  13/01/20 | L. Persampieri  |  Initial Implementation and documentation.
 *****************************************************************************/
int printContactsGraph(UniboCGRSAP* uniboCgrSap, FILE *file)
{
	int result = 0;
	ContactSAP *sap = UniboCGRSAP_get_ContactSAP(uniboCgrSap);

	if (file != NULL)
	{
		fprintf(file,
				"\n------------------------------------------------- CONTACTS GRAPH -------------------------------------------------\n");
		fprintf(file, "Time: %ld\n%-15s %-15s %-15s %-15s %-15s %-15s %s\n", (long int) UniboCGRSAP_get_current_time(uniboCgrSap),
				"FromNode", "ToNode", "FromTime", "ToTime", "XmitRate", "Confidence", "Citations");
		result = printTreeInOrder(sap->contacts, file, printContact);

		if (result == 1)
		{
			fprintf(file,
					"\n------------------------------------------------------------------------------------------------------------------\n");
		}
		else
		{
			fprintf(file,
					"\n---------------------------------------------- CONTACTS GRAPH ERROR ----------------------------------------------\n");
		}
	}
	else
	{
		result = -1;
	}

	return result;
}
